package lab2.bq1;

public class Rectangle {

//	Variable
	private double length;
	private double width;
	
//	Constructor
	public Rectangle() {
		setWidth(1.0);
		setLength(1.0);
	}

	public Rectangle(double length, double width) {
		setLength(length);
		setWidth(width);
	}
	
	
//	getter and setter
	public double getLength() {
		return length;
	}
	public void setLength(double length) {
		if(length > 0.0 && length <=40) {
			this.length = length;
			}
			else {
				System.out.println("Length is not within range!!!!");
			}
		
	}
	public double getWidth() {
		return width;
	}
	public void setWidth(double width) {
		
		if(width > 0.0 && width <=40) {
		this.width = width;
		}
		else {
			System.out.println("Width is not within range!!!!");
		}
	}
//	Misc method
	public double getArea() {
		return getLength()*getWidth();
	}
	
	public double getPerimeter(){
		return 2*(getLength()+getWidth());
	}	
	
	public void printRectangle() {
		String side = "*";
		
		for(int i=0; i<width-1; i++) {
			side+="*";
		
		}
		System.out.println(side);
		for(int j=0;j<length-2;j++) {
			System.out.print("*");
			for(int k=0;k<width-2;k++) {
				System.out.print(" ");
			}
			System.out.println("*");
		}
		System.out.print(side);
	}
//	toString()

	@Override
	public String toString() {
		return "Rectangle [length=" + length + ", width=" + width 
				+ ", Area ="+getLength()*getWidth()+", Perimeter ="
				+ 2*(getLength()+getWidth())+"]";
	}
	
	
}
