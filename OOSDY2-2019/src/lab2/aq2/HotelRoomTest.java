package lab2.aq2;
/**
 * @ roomNumber contains the room number of the hotel
 * @author Weihao Liao
 * 
 */
public class HotelRoomTest {

	public static void main(String[] args) {
		
		HotelRoom[] rooms = new HotelRoom[3];
//	Set Constructor
		HotelRoom roomA = new HotelRoom();
		roomA.setRoomNumber(200);
		roomA.setRoomType("Single");

		HotelRoom roomB = new HotelRoom(201,"Double");
		
		HotelRoom roomC = new HotelRoom(202,"Single",true, 90);
		
		roomA.setRate(100);
		roomA.setIsOccupied(true);
		
		if(roomA.isOccupied()) {
			System.out.println("This room is occupied!!!!");
		}else {
			System.out.println("This room is not occupied");
		}
		roomB.setRate(80);
		roomB.setIsOccupied(false);
		
		rooms[0]= roomA;
		rooms[1] = roomB;
		rooms[2] = roomC;
		// Print HotelRoom
		
		System.out.println(roomA.toString()+"\n"+roomB.toString());
	
		
	} //end main

} // end class
