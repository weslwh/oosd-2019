package lab2.aq2;

public class HotelRoom {
//	Variables
	private int roomNumber;
	private String roomType;
	private boolean isOccupied;
	private double rate;
	
	public boolean isOccupied() {
		return isOccupied;
	}

	public void setIsOccupied(boolean isOccupied) {
		this.isOccupied = isOccupied;
	}
	public double getRate() {
		return rate;
	}

	public void setRate(double rate) {
		this.rate = rate;
	}
	
	public HotelRoom(int roomNumber, String roomType, boolean isOccupied, double rate) {
		setRoomNumber(roomNumber);
		setRoomType(roomType);
		setIsOccupied(isOccupied);
		setRate(rate);
	}



//	Constructor 
	public HotelRoom() {
		setRoomNumber(0);
		setRoomType("");		
	}
	
// Constructor with parameters
	public HotelRoom(int roomNumber, String roomType) {
		this.roomNumber = roomNumber;
		this.roomType = roomType;
	}
	
// Getter & setter
	public int getRoomNumber() {
		return roomNumber;
	}
	public void setRoomNumber(int num) {
		this.roomNumber = num;
	}
	public int getRoomId() {
		return roomNumber;
	}
//	public void setRoomId(String Id) {
//		this.roomId = Id;
//	}
	public String getRoomType() {
		return roomType;
	}
	
	public void setRoomType(String type) {
		this.roomType = type;
	}
	

	//to String
	@Override
	public String toString() {
		return "room number is " + getRoomNumber() + ", the type is " + getRoomType()+ ", IsOccipied: "+isOccupied()+", rate "+getRate();
	}
	
	
}// end
