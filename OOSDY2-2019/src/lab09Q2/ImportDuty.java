package lab09Q2;

public interface ImportDuty {

	final double CARTAXRATE=0.1;
	final double HGVATXRATE=0.15;
	
	void calculateDuty();
	
}
