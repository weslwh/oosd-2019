package lab09Q2;

public class RoadDriver {

	public static void main(String[] args) {
		RoadVehicle[] rv = new RoadVehicle[2];
		
		Car c = new Car("BMW",4,2);
		Hgv h = new Hgv(2000,8,1);

		rv[0]=c;
		rv[1]=h;
		
		// Polymorphism
		for(RoadVehicle r: rv) {
			r.calculateDuty();
		}
	}

}
