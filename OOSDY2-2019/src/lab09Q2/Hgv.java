package lab09Q2;

public class Hgv extends RoadVehicle{
	private int cargo;

	public Hgv(){ 	
		this(0,0,0);	
	}

	public Hgv(int c, int w, int p){ 
		super(w, p);
		setCargo(c);
	}

	public void setCargo(int size){
		cargo = size;
		}

	public int getCargo(){
		return cargo;
		}

	@Override
	public void calculateDuty() {
		System.out.println("The Hgv import duty is "+ 20000*HGVATXRATE);
		
	}	
}