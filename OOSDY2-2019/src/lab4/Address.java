package lab4;

public class Address {
// Variable 
	private String street;
	private String city_town;
	private String county;
	
//	Constructor
	public Address() {
		setStreet(street);
		setCity_town(city_town);
		setCounty(county);
	}
	
	public Address(String street, String city_town, String county) {
		setStreet(street);
		setCity_town(city_town);
		setCounty(county);
	}
//	getter&setter


	public String getStreet() {
		return street;
	}


	public void setStreet(String street) {
		this.street = street;
	}


	public String getCity_town() {
		return city_town;
	}


	public void setCity_town(String city_town) {
		this.city_town = city_town;
	}


	public String getCounty() {
		return county;
	}


	public void setCounty(String county) {
		this.county = county;
	}

//	toString
	
	@Override
	public String toString() {
		return "Address [Street=" + getStreet() + ", City_town=" + getCity_town() + ", County=" + getCounty() + "]";
	}
}
