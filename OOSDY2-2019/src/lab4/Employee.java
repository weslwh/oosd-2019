package lab4;

public class Employee {
//	Variable
	private int emNum;
	private static int nextEmNum = 1000;
	private String fname;
	private String lname;
	private Address address;
	private String empType;
	private String compCarType;
	
//	Constructor
	public Employee() {
		setEmNum(nextEmNum);
		setFname(fname);
		setLname(lname);
		setAddress(address);
		setEmpType(empType);
		nextEmNum++;
	}
	
	public Employee(String fname, String lname, Address address, String empType) {
	setEmNum(nextEmNum);
	setFname(fname);
	setLname(lname);
	setAddress(address);
	setEmpType(empType);
	nextEmNum++;
}
//	getter&setter
	public int getEmNum() {
		return emNum;
	}

	public void setEmNum(int emNum) {
		this.emNum = emNum;
	}
	public String getFname() {
		return fname;
	}
	public void setFname(String fname) {
		this.fname = fname;
	}
	public String getLname() {
		return lname;
	}
	public void setLname(String lname) {
		this.lname = lname;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public String getEmpType() {
		return empType;
	}
	public void setEmpType(String empType) {
		this.empType = empType;
	}
	public String getCompCarType() {
		return compCarType;
	}
	public void setCompCarType(String compCarType) {
		this.compCarType = compCarType;
	}
	public static int getNoOfEmployee() {
		return nextEmNum-1000;
	}

	//	toString
	@Override
	public String toString() {
		if(empType.equalsIgnoreCase("Manager")){
			return "Employee [emNum=" + emNum + ", fname=" + fname + ", lname=" + lname + ", address=" + address
				+ ", empType=" + empType + ", compCarType=" + compCarType + "]";
		}else {
			return "Employee [emNum=" + emNum + ", fname=" + fname + ", lname=" + lname + ", address=" + address
					+ ", empType=" + empType + "]";
		}
	}
	
	

}
