package lab4;

import java.util.Arrays;

public class Office {
//	Variable
	private int roomnNum;
	private static int nextRoomNo = 100;
	private int numOfEmp;
	private Employee[] emps =  new Employee[2];
	
//	Constructor
	public Office() {
		setRoomnNum(nextRoomNo);
		nextRoomNo++;
	}
//	getter&setter

	public int getRoomnNum() {
		return roomnNum;
	}
	
	public void setRoomnNum(int roomnNum) {
		this.roomnNum = roomnNum;
	}

	public int getNumOfEmp() {
		return numOfEmp;
	}

	public void setNumOfEmp(int numOfEmp) {
		this.numOfEmp = numOfEmp;
	}

//	MIsc method
	public void addEmployee(Employee emp) {
		if(numOfEmp <=2) {
			emps[numOfEmp] = emp;
			numOfEmp++;
		}
		else {
			System.out.println("number of Employee reached maximum");
		}
	}
	
	public String printEmployee() {
		String empdetails = "";
		for(Employee emp: emps) {
			if(emp!=null) {
				empdetails += emp.getEmNum()+","+emp.getFname()+","+emp.getLname()+"\n";
			}
		}
		return empdetails;
	}
	
//	toString
	
	@Override
	public String toString() {
		return "Office [room Number=" + getRoomnNum() + ", Number Of Employee=" + getNumOfEmp() + "]";
	}

	
}
