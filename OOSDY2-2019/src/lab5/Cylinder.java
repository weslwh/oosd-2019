package lab5;

public class Cylinder extends Circle {
//	variable 
	protected float height;
	
//	constructor
	public Cylinder(int x, int y, float rad, float hi) {
		super(x, y, rad);
		setHeight(hi);
	}

//	getters&setters
	
	public float getHeight() {
		return height;
	}

	public void setHeight(float height) {
		this.height = height;
	}

	
//	toString
	
	@Override
	public String toString() {
		return super.toString()+" height: "+ getHeight();
		//return "Cylinder [height=" + height + ", radius=" + radius + ", x=" + x + ", y=" + y + "]";
	}


	

	

	
}// end class
