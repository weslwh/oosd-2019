package lab5;

public class MyFirstCircle
{
		public static void main (String args[])
		{
			Point myPoint = new Point(10, 20);

			Circle myCircle = new Circle(15, 30, 5f);
			
			Cylinder mycylinder = new Cylinder(2,3,10f,3f);

			System.out.println("Point details : " + myPoint);
			
			System.out.println("Circle details: " + myCircle);

			System.out.println("Circle details: " + mycylinder);
		}
}
