package lab5;

public class Circle extends Point {
//	Variable
	protected float radius;
	
// Constructor


	public Circle(int x, int y, float rad) {
		super(x, y);
		setRadius(rad);
	}

// getters & setters
	public float getRadius() {
		return radius;
	}


	public void setRadius(float radius) {
		this.radius = radius;
	}

// toString
	@Override
	public String toString() {
		return super.toString() +" Radius: "+ getRadius();
//			return "Circle [radius=" + radius + ", x=" + x + ", y=" + y + "]";
	}

	
	
}
// end class
