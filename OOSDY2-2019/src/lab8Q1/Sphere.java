package lab8Q1;

public class Sphere extends ThreeDShape {
// Variable
	double radius;
	
//	Constructor
	public Sphere(String name, String colour, double radius) {
		super(name, colour);
		setRadius(radius);
	}
	
//	getter&setter
	public double getRadius() {
		return radius;
	}

	public void setRadius(double radius) {
		this.radius = radius;
	}
	
//	Misc Method

	@Override
	public double volume() {
		return Math.PI*(4/3)*Math.pow(radius,3);
	}



	@Override
	public double area() {
		return 4*Math.PI*Math.pow(radius, 2);
	}

}
