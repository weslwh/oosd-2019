package lab8Q1;

public class Circle extends TwoDShape {

//	Variable	
	double radius;

//	Constructor
	public Circle(String name, String colour, double radius) {
		super(name, colour);
		setRadius(radius);
	}

//	getter&setter
	
	public double getRadius() {
		return radius;
	}

	public void setRadius(double radius) {
		this.radius = radius;
	}

		
//	Misc Method
	@Override
	public double area() {
		return Math.pow(radius,2)*Math.PI;
	}
// 	ToString
	
	@Override
	public String toString() {
		return super.toString()+"\n"+"Radius = " + radius;
	}


	

}
