package lab8Q1;

public class Rectangle extends TwoDShape{


//	Variable	
	double length;
	double breadth;

//	Constructor
	public Rectangle(String name, String colour, double length, double breadth) {
		super(name, colour);
		setLength(length);
		setBreadth(breadth);
	}

//	getter&setter
	
	public double getLength() {
		return length;
	}

	public void setLength(double length) {
		this.length = length;
	}

	public double getBreadth() {
		return breadth;
	}

	public void setBreadth(double breadth) {
		this.breadth = breadth;
	}
	
//	Misc Method
	@Override
	public double area() {
		return (length*breadth)/2;
	}
	
// 	ToString
	@Override
	public String toString() {
		return super.toString()+"\n"+"Length = " + length + "\n"+"Breadth = " + breadth;
	}

	


	

}