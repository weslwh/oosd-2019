package lab8Q1;

public class Triangle extends TwoDShape {
//	Variable
	double base, height;
	
//	Constructor
	public Triangle(String name, String colour, double base, double height) {
		super(name, colour);
		setBase(base);
		setHeight(height);
		
	}
//	getter&setter
	public double getBase() {
		return base;
	}



	public void setBase(double base) {
		this.base = base;
	}



	public double getHeight() {
		return height;
	}



	public void setHeight(double height) {
		this.height = height;
	}
	
	
// Misc method
	@Override
	public double area() {
		return 0.5*base*height;
	}

	
// 	ToString
	@Override
	public String toString() {
		return super.toString()+"\n"+"Base = " + base + "\n"+"Height = " + height;
	}

}
