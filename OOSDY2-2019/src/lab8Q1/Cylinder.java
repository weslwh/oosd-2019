package lab8Q1;

public class Cylinder extends ThreeDShape {

	
//	Variable
	double radius,height;
//	Constructor
	public Cylinder(String name, String colour,double radius, double height) {
		super(name, colour);
		this.radius = radius;
		this.height = height;
	}

	
//	Misc Method
	@Override
	public double volume() {
		return 2*radius*height;
	}

	
	
	@Override
	public double area() {
		return Math.PI*Math.pow(radius, 2)*height;
	}

//	toString
	
	@Override
	public String toString() {
		return super.toString() +"\n"+"Radius = " + radius + "\n"+"Height = " + height;
	}

	
	
}
