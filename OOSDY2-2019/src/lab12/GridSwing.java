package lab12;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.JCheckBox;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.font.*;


public class GridSwing extends JFrame{
	private JTextField textField;
	private JCheckBox SnapJCheckBox;
	private JCheckBox ShowJCheckBox;
	private JLabel label1;
	private JButton okJButton;
	private JButton cancelJButton;
	private JButton helpJButton;
	JPanel form = new JPanel();
	JPanel flow = new JPanel();
	JPanel buttonform = new JPanel();
	JPanel buttons = new JPanel();
	Border padding1;
	public static Box createHorizontalBox() {
		Box lineBox = new Box(BoxLayout.LINE_AXIS);
		return lineBox;
	}

	public GridSwing() {
		super("Align");
		getContentPane().setLayout(new BorderLayout());
		flow.setLayout(new GridLayout(2,1));
		form.setLayout(new FlowLayout());
		buttonform.setLayout(new GridLayout(3,1));
		//padding1 = BorderFactory.createEmptyBorder(10, 30, 10, 30); // form panel padding
		form.setBorder(padding1);
		
		SnapJCheckBox = new JCheckBox("Snap to Grid ");
		form.add(SnapJCheckBox);

		
		textField = new JTextField("",4);
		textField.setFont(new Font("Serif", Font.PLAIN, 14));
		label1 = new JLabel("X:"+textField.getText());
		form.add(label1);
		form.add(textField);
	

		
		ShowJCheckBox = new JCheckBox("Show Grid");
		form.add(ShowJCheckBox);
		
		
		
		Box box = Box.createHorizontalBox( );
		box.setSize(30, 30);
		box.setPreferredSize(new Dimension(10,2));		
		form.add(box);
		
		label1 = new JLabel("Y:");
		textField = new JTextField("",4);
		form.add(label1);
		
		textField.setFont(new Font("Serif", Font.PLAIN, 14));
		form.add(textField);


		okJButton = new JButton("OK");
		buttonform.add(okJButton);
		
		cancelJButton = new JButton("Cancel");
		buttonform.add(cancelJButton);
		
		helpJButton = new JButton("Help");
		buttonform.add(helpJButton);


		add(form, BorderLayout.CENTER);
		add(buttonform, BorderLayout.EAST);
	}
	
	
	
	
}
