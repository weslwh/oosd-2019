package lab09Q1;

public class Book extends LibraryItem {
	
// Variable
	String author, title;
	int numPages;
// Constructor
	public Book(String type, String id, String author, String title, int numPages) {
		super(type, id);
		setAuthor(author);
		setTitle(title);
		setNumPages(numPages);
		
}
	
//	Getter&Setter
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getNumPages() {
		return numPages;
	}
	public void setNumPages(int numPages) {
		this.numPages = numPages;
	}

	@Override
	public void calculatePrice() {
		System.out.println("The Book Price:"+(getNumPages()*2/10));
	
	}
		
}//end class