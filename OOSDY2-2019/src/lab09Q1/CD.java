package lab09Q1;

public class CD extends LibraryItem {
//	variable
	String band,title;
	int numTracks;

//	Constructor
	public CD(String type, String id, String band,String title, int numTrack) {
		super(type,id);
		setBand(band);
		setTitle(title);
		setNumTracks(numTrack);
		
	}
	
//	Getter&setter
	public String getBand() {
		return band;
	}

	public void setBand(String band) {
		this.band = band;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getNumTracks() {
		return numTracks;
	}

	public void setNumTracks(int numTracks) {
		this.numTracks = numTracks;
	}

	@Override
	public void calculatePrice() {
		System.out.println("The CD price is:"+(getNumTracks()/20));
		
	}
	
	
}
