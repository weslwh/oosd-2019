package lab09Q1;

public abstract class LibraryItem extends LoanItem {
	
//	Variable
	String Type;
	String Id;
//	Constructor 
	public LibraryItem(String type, String id) {
	this.Type = type;
	this.Id = id;
}
//	getter&setter
	public String getType() {
		return Type;
	}



	public void setType(String type) {
		Type = type;
	}

	public String getID() {
		return Id;
	}

	public void setID(String iD) {
		Id = iD;
	}
	
	@Override
	public void calculatePrice() {
		System.out.println();
	}


	
}
