package lab09Q1;

public class LibraryItemsDriver {

	public static void main(String[] args) {
		LibraryItem[] li = new LibraryItem[2];
		
		Book b = new Book("Book","Book001","Kevin","Java",100);
		CD c = new CD("CD","CD001","Beatles","hello",500);
		
		li[0]=b;
		li[1]=c;
		
		for(LibraryItem i:li) {
			i.calculatePrice();
		}
		
	}//end main

}//end class
