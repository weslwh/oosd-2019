package lab3.q2q3;

public class SavingsAccount {
	
//Variable
private int accountNum;	// 1 per object instance
private static int nextacc=0;	// 1 shared by all object instance
private static float annualinterestrate= 0f;
private double balance;

//Constructor
public SavingsAccount(){
	nextacc++;
	setAccountNum(nextacc);
}


public SavingsAccount(double balance) {
	nextacc++;
	setAccountNum(nextacc);
	setBalance(balance);
}


public static float getAnnualinterestrate() {
	return annualinterestrate;
}
public static void modifyAnnualinterestrate(float annualinterestrate) {
	SavingsAccount.annualinterestrate = annualinterestrate;
}
public double getBalance() {
	return balance;
}
public void setBalance(double balance) {
	this.balance = balance;
}

//Getter & Setter
public int getAccountNum() {
	return accountNum;
}

public void setAccountNum(int accountNum) {
	this.accountNum = accountNum;
}

// Misc Methods
public void calculateMonthlyInterest() {
	double interest = getBalance() * getAnnualinterestrate()/12;
	setBalance(getBalance()+interest);
}


//toString
@Override
public String toString() {
	return "SavingAccount [accountNum=" + accountNum + ", balance=" + balance + "]";
}

	
//End class
}
