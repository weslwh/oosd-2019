package lab3.q2q3;

public class BankCustomerDriver {

	public static void main(String[] args) {
		
		BankCustomer bc  = new BankCustomer("John", "Cork");
		SavingsAccount sa1 = new SavingsAccount(2000.00);
		bc.addAccount(sa1);
		
		
		SavingsAccount sa2 = new SavingsAccount(3000.00);
		bc.addAccount(sa2);
		
		
		SavingsAccount sa3 = new SavingsAccount(4000.00);
		bc.addAccount(sa3);
		
		System.out.println(bc);
		
		for(SavingsAccount sav: bc.getSaveacc()) {
			if(sav!=null) {
				System.out.println("Account No: "+ sav.getAccountNum()+"\t"+"balance: "+ sav.getBalance());
			}
		}
		System.out.println("Total balance: "+bc.balance(bc.getSaveacc()));
	}

}
