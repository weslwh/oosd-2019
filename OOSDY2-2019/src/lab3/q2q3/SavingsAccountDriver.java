package lab3.q2q3;

public class SavingsAccountDriver {

	public static void main(String[] args) {
		
		SavingsAccount saver1 = new SavingsAccount();
		saver1.setBalance(2000.00);
		
		SavingsAccount saver2= new SavingsAccount(3000.00);
		
		SavingsAccount.modifyAnnualinterestrate(0.04f);
		
		saver1.calculateMonthlyInterest();
		saver2.calculateMonthlyInterest();
		
		System.out.println(saver1);
		System.out.println(saver2);
		
		
		
		SavingsAccount.modifyAnnualinterestrate(0.05f);
		
		saver1.calculateMonthlyInterest();
		saver2.calculateMonthlyInterest();
		
		System.out.println(saver1);
		System.out.println(saver2);

	} //end main

}// end class 
