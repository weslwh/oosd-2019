package lab3.q2q3;

import java.util.Arrays;

public class BankCustomer {
// Variable
	private String name;
	private String address;
	private SavingsAccount[] saveacc = new SavingsAccount[3];
	private int noaccs;
// Constructor


	public String getName() {
		return name;
	}
	public BankCustomer(String name, String address) {
	this.name = name;
	this.address = address;
	}
	
// getter& setter
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public SavingsAccount[] getSaveacc() {
		return saveacc;
	}
	public void setSaveacc(SavingsAccount[] saveacc) {
		this.saveacc = saveacc;
	}
	
	public int getNoacc() {
		return noaccs;
	}
	public void setNoacc(int noacc) {
		this.noaccs = noaccs;
	}

	
//	Misc method
	public void addAccount(SavingsAccount savingsaccount) {
		if(getNoacc()<=2) {
			this.saveacc[getNoacc()]= savingsaccount;
			noaccs++;
		}
		else {
			System.out.println("max account reached!");
		}
	}
	public double balance(SavingsAccount[] savingsaccounts) {
		double totalbalance=0;
		for(SavingsAccount sa: savingsaccounts) {
			if(sa!=null) {
				totalbalance+=sa.getBalance();
			}
		}
		return totalbalance;
	}
//	toString
	@Override
	public String toString() {
		return "BankCustomer [name=" + name + ", address=" + address + "]";
	}

	
	
}
