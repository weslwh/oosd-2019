package lab3.q1;

import java.util.Calendar;

public class Clock {

	public static void main(String[] args) {
		
		Calendar cal = Calendar.getInstance();
		
		Time t = new Time(cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE));
		System.out.println(t.toString());
		
		long starttime = System.currentTimeMillis();
		long currenttime = starttime;
		int oldmin = cal.get(Calendar.MINUTE);
		int newmin = oldmin;
		
		while (newmin == oldmin) {
			while((currenttime -starttime) < 1000) // wait for one second before call the method
			{
				currenttime = System.currentTimeMillis();
			}// inner while loop
			
		
		t.tick();
		System.out.println(t.toString());
		newmin = t.getMinute();
		starttime = System.currentTimeMillis();
		currenttime = starttime;
		}//end outer while
	}//end main

}
