package lab15;

public class Verifier {

	public static void verifyIntRange(int value, int low, int high) throws MyOutOfRangeException {

		if (value < low || value > high)
			throw new MyOutOfRangeException("Value is not in the range 1-100");
		else {
			System.out.println("Number in range!!!");
		}

	}

	public static void verifyPasswordStrength(String password) throws MyInvalidPasswordException {
		if (password.length() < 8)
			throw new MyInvalidPasswordException(password + " is not strong enough!");
		else {
			System.out.println("Good password.");
		}
	}
}// end class