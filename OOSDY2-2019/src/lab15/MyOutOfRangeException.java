package lab15;

public class MyOutOfRangeException extends Exception {
	public MyOutOfRangeException(String msg) {
		super(msg);
	}
}
