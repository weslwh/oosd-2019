package lab8Q2;

import lab09Q1.LibraryItem;

public class TestPerson {
	public static void main(String[] args) {
		Person[] person = new Person[2];
		Employee employee = new Employee("Karen", 10000);
		Student student = new Student("Wesley", "Software Developement");

		person[0]= employee;
		person[1]= student;
		for(Person p: person) {
		System.out.println(p.getDescription()+"\n");
		}
	}

}
