package lab8Q2;

public class Student extends Person {
//	Variable
	String course;
	public Student(String name, String course) {
		super(name);
		this.course = course;
	}

	@Override
	public String getDescription() {
		return  super.getName()+"\n"+"An student studying "+course;
	}

}

