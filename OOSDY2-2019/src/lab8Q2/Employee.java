package lab8Q2;

public class Employee extends Person {
//	Variable
	double annualSalary;
	public Employee(String name, double annualSalary) {
		super(name);
		this.annualSalary = annualSalary;
	}

	@Override
	public String getDescription() {
		return super.getName()+"\n"+"An employee with a salary of "+"�"+annualSalary;
	}

}
