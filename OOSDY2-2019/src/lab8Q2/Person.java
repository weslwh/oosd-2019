package lab8Q2;

public abstract class Person {

//	Variable
	String name;
	
//	constructor
	public Person(String name) {
		this.name = name;
	}

//	Misc method
	public String getName() {
		return "The Person's name is: "+name;
	}
	
	public abstract String getDescription();
}
