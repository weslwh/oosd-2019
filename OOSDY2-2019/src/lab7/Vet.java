package lab7;

public class Vet {
	
//	Variable
	private String name;
	
	
//	Constructor
public Vet(String name) {
	this.name = name;
}
	
//	getter&setter
public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}

//	Misc method
	public void Vaccinate(Animal[] animal) {
		for(Animal a: animal) {
			System.out.println(this.name+" is vaccinating");
			System.out.println(a.getClass().getName().substring(5,8));
			System.out.println("The "+a.getClass().getName().substring(5,8)+" has been vaccinated "+a.toString());
		}
	} 
//	toString

	@Override
	public String toString() {
		return "Vet [name=" + name + "]"+"is vaccinating.";
	}

	
}
