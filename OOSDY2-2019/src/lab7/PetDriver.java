package lab7;

public class PetDriver {

	public static void main(String[] args) {

		Animal dog = new Dog("Husky",2,'M');
		Animal cat = new Cat("Cheshire",2,'F');
		Animal cow = new Cow("Freezin",5,'M');
		
		Animal[] animals = new Animal[3];
		animals[0] = dog;
		animals[1] = cat;
		animals[2] = cow;
		
//		Polymorphism
		for(Animal i: animals) {
			i.eat();
			
		}
		Vet v = new Vet("Tom");
		v.Vaccinate(animals);
		
	}//	end main

}//	end class
