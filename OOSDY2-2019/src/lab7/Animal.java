package lab7;

public abstract class Animal {

//	variable
	protected String type;
	protected int age;
	protected char gender;
	
//	Constructor
	public Animal(String type, int age,char gender) {
		setType(type);
		setAge(age);
		setGender(gender);
	}
//	getter & setter

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public char getGender() {
		return gender;
	}

	public void setGender(char gender) {
		this.gender = gender;
	}
	
//	Misc Method
	public abstract void eat();
	public abstract void sleep();
	public abstract void makeSound();

//	toString
	@Override
	public String toString() {
		return "Animal [type=" + type + ", age=" + age + ", gender=" + gender +"]";
	}
	

	
	
}
