package lab6;

import java.util.Arrays;

public class Teacher extends Person{
// variable
	private int numCourses;
	private String[] Courses;
	private static int MAXCOURSES=10;

//	constructor
	public Teacher(String name, String address) {
		super(name, address);
		numCourses = 0;
		Courses = new String[MAXCOURSES];
		// TODO Auto-generated constructor stub
	}
	
//	getter &setter
	
//	Misc Methods
	public boolean addCourse(String course) {
		for(int i=0;i<numCourses;i++) {
			if(Courses[i].equals(course)) {
				return false;
			}
		}
		Courses[numCourses]=course;
		numCourses++;
		return true;
	}
	
	public boolean removeCourse(String course) {
		int courseindex = numCourses;
		for(int i=0;i<=numCourses;i++) {
			if(Courses[i].equals(course)) {
				courseindex=1;
				break;
			}
		}
		if(courseindex == numCourses) {
			return false;
		}
		else {
			for(int i=0;i<=numCourses;i++) {
				Courses[i]=Courses[i+1];
				return true;
			}
			numCourses--;
			return true;
		} 
	}
//toString

	@Override
	public String toString() {
		return "Teacher [numCourses=" + numCourses + ", Courses=" + Arrays.toString(Courses) + ", toString()="
				+ super.toString() + "]";
	}
	
}
