package lab6;

public class Student extends Person{
//	variable
	private int numCourses;
	private String[] courses;
	private int[] grades;
	private static int MAXCOURSES = 10;

//	Constructor
	public Student(String name, String address) {
		super(name, address);
		numCourses = 0;
		courses = new String[MAXCOURSES];
		grades = new int[MAXCOURSES];
	}
	
//	getter&setter
	
//	Misc method
	public void addCourseGrade(String course,int grade) {
		courses[numCourses] = course;
		grades[numCourses] = grade;
		numCourses++;
	}
	public void printGrades() {
		System.out.print(this);
		for(int i=0;i<=courses.length-1;i++) {
			System.out.println("Courses: "+courses[i]+"\t" + "Grade");
		}
	}
	
	public double getAverageGrade() {
		int sum = 0;
		for(int calllthisanything:grades)sum+= calllthisanything;
			return sum/numCourses;
		
	}
//	ToString
}
// end class
