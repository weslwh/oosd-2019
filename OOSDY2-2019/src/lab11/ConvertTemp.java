package lab11;

//Converts a Fahrenheit temperature entered by the user to Celsius, or vice versa

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class ConvertTemp extends JFrame {

	private JTextField fahrenField = new JTextField();
	private JTextField celsiusField = new JTextField();

	// Constructor
	public ConvertTemp(String title) {
		// Set title for frame and choose layout
		super(title);
		getContentPane().setLayout(new GridLayout(2, 2));

		// Add Fahrenheit label and text field to frame
		add(new JLabel("Fahrenheit"));
		add(fahrenField);
		// Attach listener to text field

		// Add Celsius label and text field to frame
		add(new JLabel("Celsius"));
		add(celsiusField);
		// Attach listener to text field

		// Attach window listener
		addWindowListener(new WindowCloser());
	// end costructor

	// private inner class for event handling
	fahrenField.addActionListener (new ActionListener(){
		public void actionPerformed(ActionEvent event) {
			double fahrenheit = Double.parseDouble(fahrenField.getText());
			double result = ((fahrenheit-32)*5)/9;
			String Ceresult = String.valueOf(result);
			celsiusField.setText(Ceresult);
			
		}
	});
	}

	// Listener for window
	class WindowCloser extends WindowAdapter {
		public void windowClosing(WindowEvent evt) {
			System.exit(0);
		}
	}

}// end class