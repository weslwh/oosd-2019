package lab13;

import java.awt.BorderLayout;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class ColourSelectFrame extends JFrame {

	private JButton ok;
	private JButton cancel;
	private JCheckBox background;
	private JCheckBox foreground;
	private JComboBox colour;
	private JPanel checkpanel;
	private JPanel buttonpanel;
	
	public ColourSelectFrame() {
		
		super("ColourSelect");
		setLayout(new BorderLayout());
		
		//combobox
		colour = new JComboBox();
		colour.addItem("RED");
		colour.addItem("BLUE");
		colour.addItem("GREEN");
		add(colour, BorderLayout.NORTH);
		
		//checkbox
		background = new JCheckBox("background");
		foreground = new JCheckBox("foreground");
		checkpanel = new JPanel();
		checkpanel.add(background); //add CheckBox to the panel
		checkpanel.add(foreground);
		add(checkpanel, BorderLayout.CENTER);
		
		//buttons
		ok = new JButton("Ok");
		cancel = new JButton("Cancel");
		buttonpanel = new JPanel();
		buttonpanel.add(ok);
		buttonpanel.add(cancel);
		add(buttonpanel, BorderLayout.SOUTH);
			
	}
	
	
	
}
